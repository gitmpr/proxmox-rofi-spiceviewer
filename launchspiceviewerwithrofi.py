#!/usr/bin/python3

# authenticate to Proxmox VE with API token, which is stored in plaintext in this file
# query list of VMs from Proxmox API
# open rofi selection menu with list of VMs to choose from
# open spice viewer client to the selected VM

# Copyright Michael Petrus Roelofsen 2021

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program. If not, see <https://www.gnu.org/licenses/>. 

# https://pve.proxmox.com/wiki/Proxmox_VE_API
# https://pve.proxmox.com/pve-docs/api-viewer/index.html
# https://thinklab.mpr.lan:8006
# https://thinklab.mprlan.tech
# https://thinklab.mpr.lan:8006/pve-docs/chapter-pvesh.html
# https://pve.proxmox.com/pve-docs/api-viewer/index.html#/access/users/{userid}/token/{tokenid}

# TODO 
# venv/virtualenv met python rofi
# depencencies handling, pip install
# env vars for password and/or API key
# implement as classes
# separate my own hostnames, node names, etc. completely
# read credentials or API key from separate file
# user input for authentication
# detect if spiceviewer is installed
# error handling

import subprocess
import argparse
import requests
import json
from colorama import init, deinit, Fore, Style, Back
from pygments import highlight, lexers, formatters
from rofi import Rofi
from pygments import highlight
from pygments.lexers import PythonLexer
from pygments.formatters import Terminal256Formatter
import time
from sys import exit


import pprint
from pprint import pformat
pp = pprint.pprint




node_url = 'proxmox.mprlan.tech' # make sure hostname is also set right in /etc/hosts on proxmox node
node_port = '443' # 8006 by default, 443 if reverse proxied through https
node = 'thinklab'


username    = "root@pam"
# token_id  = "rofi_token"
# token_value = ""




# import local auth config
try:
    from api_token_and_key import token_id, token_value
    print('using api token auth')
    authheaders = {
        "Authorization" : F"PVEAPIToken={username}!{token_id}={token_value}"
    }
    
except ImportError:
    try:
        from username_and_password import username, password
        print('using password auth')
        
        access_request = requests.post(F"https://{node}:8006/api2/json/access/ticket", 
            verify=False,     
            data = {"username": username,
                    "password": password
            }
        )
        if access_request:
            print('auth OK')
        
        CSRFPrefentionToken = access_request.json()['data']['CSRFPreventionToken']
        ticket = access_request.json()['data']['ticket']

        print("CSRFPrefentionToken = " + CSRFPrefentionToken)
        print("ticket = " + ticket)
        print()
        
        authheaders = {
            "CSRFPreventionToken": CSRFPrefentionToken,
            "Cookie": "PVEAuthCookie=" + ticket
        }
        
    except ImportError:
        print('No authentication method specified. Configure api token or password auth from EXAMPLE config files')
        exit()


#parser = argparse.ArgumentParser()
#parser.add_argument("-u", "--username", type=str, default="root@pam",  help="username that you log in with on proxmox webpage dashboard. Uses root@pam if not specified")
#parser.add_argument("-p", "--password", type=str, default="",          help="password to login on proxmox webpage dashboard")
#parser.add_argument("vmid",             type=int, default=100,         help="id for VM")
#parser.add_argument("node",             type=str, default="localhost", help="Proxmox cluster node name")
#parser.add_argument("-P", "--proxy",    type=str, default=None,        help="proxy: DNS or IP (use <node> as default)")
#args = parser.parse_args()

# pretty print json
def ppjs(dict_or_string):
    if (type(dict_or_string) == dict):
        # print("is dict")
        json_dict=dict_or_string
    elif (type(dict_or_string) == str):
        # print("is string")
        json_string=dict_or_string
        json_dict = json.loads(json_string)
    else:
        raise ValueError('invalid argument type for prettyprint json function')
    jdump=json.dumps(json_dict, indent=2) # convert to indented json string
    colorful_json = highlight(jdump, lexers.JsonLexer(), formatters.Terminal256Formatter(style='monokai'))
    print(colorful_json) 

# print response status and optionally reponse body of http request
def request_log(request, printresponse=True, appendnewline=True):
    print(Fore.YELLOW + request.url +  Style.RESET_ALL)
    print('HTTP status code ' + str(request.status_code))
    if request:
        print('request successful')     
    else:
        print('An error has occured in the HTTP ' + request.request.method + ' request')     
    if printresponse:
        print(request.text)        
    if appendnewline: print()



resources_request = requests.get(f"https://{node_url}:{node_port}/api2/json/cluster/resources?type=vm",    # port 443 instead of 8006 if through reverse proxy
    verify=False,
    headers = authheaders
)
#request_log(resources_request)

vms = json.loads(resources_request.text)['data']


rofi_list = []
row_list = []
for vm in vms:
    rofi_item = str(vm['vmid']) + ': ' + vm['name'] + ' -- ' + vm['status']
    rofi_list.append(rofi_item)
    
    vm_fields = [ vm['vmid'], vm['name'], vm['status'] ]
    rofi_entry = "{: >5}{: >20}{: >20}".format(*vm_fields)    
    row_list.append(rofi_entry)

r = Rofi()

answer, key = r.select('select VM', row_list)

# print(row_list[answer]) # debug


# spice_request = requests.post(f"https://{node_url}:8006/api2/spiceconfig/nodes/{node}/qemu/{args.vmid}/spiceproxy", 

def run_spice_viewer():
    spice_request = requests.post(f"https://{node_url}:{node_port}/api2/spiceconfig/nodes/thinklab/qemu/{vms[answer]['vmid']}/spiceproxy",  
        verify=False,
        headers = authheaders
    )
    request_log(spice_request)

    text_file = open("spiceproxy0", "w")
    n = text_file.write(spice_request.text)
    text_file.close()

    subprocess.run(['remote-viewer', "spiceproxy0"])



if answer >= 0 and key == 0: # key = 0 is enter, key = -1 is escape; answer = -1 if enter is pressed on empty search results list with no matching strings or when escape is pressed. Answer is 0 or higher if an item is selected
    print(F'{key = }')
    print(F'{answer = }')
    
    selected_vm = vms[answer]
    vmid = selected_vm['vmid']
    
    print('selected_vm = ')
    ppjs(selected_vm)
    
    if selected_vm['status'] == 'running':
        run_spice_viewer()
        
    else:
        r2 = Rofi()
        answer2, key2 = r2.select('VM is not running. Start it up now?', ['Yes', 'No'])
        print(F'{answer2 = }') # Yes = 0 ; No = 1; Escape/Cancel = -1
        print(F'{key2 = }') # Enter = 0; Escape/Canel = -1

        if answer2 == 0 and key2 == 0:
            resources_request = requests.post(f"https://{node_url}:8006/api2/json//nodes/{node}/qemu/{vmid}/status/start", 
                verify=False,
                headers = authheaders
            )
            time.sleep(1) # wait for VM to come up
            run_spice_viewer()
        

